﻿
// // function LoadPowerData() {
// //     $('#chart_power').highcharts({
// //         chart: {
// //             type: 'spline'
// //         },
// //         title: {
// //             text: ''
// //         },
// //         subtitle: {
// //             text: ''
// //         },
// //         xAxis: {
// //             type: 'datetime',
// //             title: {
// //                 text: '日期'
// //             },
// //             dateTimeLabelFormats: {
// //                day: '%m/%d'
// //             }
// //         },
// //         colors: ['#5AB9A4', '#39F', '#06C', '#036', '#000'],
// //         yAxis: {
// //             title: {
// //                 text: ''
// //             },
// //             min: 0
// //         },
// //         tooltip: {
// //             headerFormat: '<b>{series.name}</b><br>',
// //             pointFormat: '{point.x:%Y/%m/%d}: {point.y:.0f}'
// //         },
// //         plotOptions: {
// //             spline: {
// //                 marker: {
// //                     enabled: true
// //                 }
// //             }
// //         },
// //         legend: {
// //             layout: 'vertical',
// //             align: 'right',
// //             verticalAlign: 'middle'
// //         },
// //         series: [
// //        {
// //            name: '电量（度）',
// //            data: [
// //                [Date.UTC(2018, 9, 1),23],
// //                [Date.UTC(2018, 9, 2), 6],
// //                [Date.UTC(2018, 9, 3), 34],
// //                [Date.UTC(2018, 9, 4), 21],
// //                [Date.UTC(2018, 9, 5), 14],
// //                [Date.UTC(2018, 9, 6), 53],
// //                [Date.UTC(2018, 9, 7), 21],
// //                [Date.UTC(2018, 9, 8), 34],
// //                [Date.UTC(2018, 9, 9), 12],
// //                [Date.UTC(2018, 9, 10), 12],
// //                [Date.UTC(2018, 9, 11), 53],
// //                [Date.UTC(2018, 9, 12), 34],
// //                [Date.UTC(2018, 9, 13), 65],
// //                [Date.UTC(2018, 9, 14), 85],
// //                [Date.UTC(2018, 9, 15), 43],
// //                [Date.UTC(2018, 9, 16), 75],
// //                [Date.UTC(2018, 9, 17), 12],
// //                [Date.UTC(2018, 9, 18), 64],
// //                [Date.UTC(2018, 9, 19), 75],
// //                [Date.UTC(2018, 9, 20), 34],
// //                [Date.UTC(2018, 9, 21), 90],
// //                [Date.UTC(2018, 9, 22), 45],
// //                [Date.UTC(2018, 9, 23), 63],
// //                [Date.UTC(2018, 9, 24), 25],
// //                [Date.UTC(2018, 9, 25), 53],
// //                [Date.UTC(2018, 9, 26), 43],
// //                [Date.UTC(2018, 9, 27), 32],
// //                [Date.UTC(2018, 9, 28), 20],
// //                [Date.UTC(2018, 9, 29), 45],
// //                [Date.UTC(2018, 9, 30), 13],
// //                [Date.UTC(2018, 9, 31), 23]
// //            ]
// //        }]
// //     });
// // }
// // function LoadGasData() {
// //     $('#chart_gas').highcharts({
// //         chart: {
// //             type: 'spline'
// //         },
// //         title: {
// //             text: ''
// //         },
// //         subtitle: {
// //             text: ''
// //         },
// //         xAxis: {
// //             type: 'datetime',
// //             title: {
// //                 text: '日期'
// //             },
// //             dateTimeLabelFormats: {
// //                 day: '%m/%d'
// //             }
// //         },
// //         colors: ['#5AB9A4', '#39F', '#06C', '#036', '#000'],
// //         yAxis: {
// //             title: {
// //                 text: ''
// //             },
// //             min: 0
// //         },
// //         tooltip: {
// //             headerFormat: '<b>{series.name}</b><br>',
// //             pointFormat: '{point.x:%Y/%m/%d}: {point.y:.0f}'
// //         },
// //         plotOptions: {
// //             spline: {
// //                 marker: {
// //                     enabled: true
// //                 }
// //             }
// //         },
// //         legend: {
// //             layout: 'vertical',
// //             align: 'right',
// //             verticalAlign: 'middle'
// //         },
// //         series: [
// //        {
// //            name: '燃气（L）',
// //            data: [
// //                [Date.UTC(2018, 9, 1), 23],
// //                [Date.UTC(2018, 9, 2), 6],
// //                [Date.UTC(2018, 9, 3), 34],
// //                [Date.UTC(2018, 9, 4), 21],
// //                [Date.UTC(2018, 9, 5), 14],
// //                [Date.UTC(2018, 9, 6), 53],
// //                [Date.UTC(2018, 9, 7), 21],
// //                [Date.UTC(2018, 9, 8), 34],
// //                [Date.UTC(2018, 9, 9), 12],
// //                [Date.UTC(2018, 9, 10), 12],
// //                [Date.UTC(2018, 9, 11), 53],
// //                [Date.UTC(2018, 9, 12), 34],
// //                [Date.UTC(2018, 9, 13), 65],
// //                [Date.UTC(2018, 9, 14), 85],
// //                [Date.UTC(2018, 9, 15), 43],
// //                [Date.UTC(2018, 9, 16), 75],
// //                [Date.UTC(2018, 9, 17), 12],
// //                [Date.UTC(2018, 9, 18), 64],
// //                [Date.UTC(2018, 9, 19), 75],
// //                [Date.UTC(2018, 9, 20), 34],
// //                [Date.UTC(2018, 9, 21), 90],
// //                [Date.UTC(2018, 9, 22), 45],
// //                [Date.UTC(2018, 9, 23), 63],
// //                [Date.UTC(2018, 9, 24), 25],
// //                [Date.UTC(2018, 9, 25), 53],
// //                [Date.UTC(2018, 9, 26), 43],
// //                [Date.UTC(2018, 9, 27), 32],
// //                [Date.UTC(2018, 9, 28), 20],
// //                [Date.UTC(2018, 9, 29), 45],
// //                [Date.UTC(2018, 9, 30), 13],
// //                [Date.UTC(2018, 9, 31), 23]
// //            ]
// //        },
// //        {
// //            name: '氧气(L)',
// //            data: [
// //                [Date.UTC(2018, 9, 1), 13],
// //                [Date.UTC(2018, 9, 2), 26],
// //                [Date.UTC(2018, 9, 3), 15],
// //                [Date.UTC(2018, 9, 4), 34],
// //                [Date.UTC(2018, 9, 5), 23],
// //                [Date.UTC(2018, 9, 6), 21],
// //                [Date.UTC(2018, 9, 7), 13],
// //                [Date.UTC(2018, 9, 8), 24],
// //                [Date.UTC(2018, 9, 9), 9],
// //                [Date.UTC(2018, 9, 10), 23],
// //                [Date.UTC(2018, 9, 11), 21],
// //                [Date.UTC(2018, 9, 12), 19],
// //                [Date.UTC(2018, 9, 13), 8],
// //                [Date.UTC(2018, 9, 14), 34],
// //                [Date.UTC(2018, 9, 15), 26],
// //                [Date.UTC(2018, 9, 16), 13],
// //                [Date.UTC(2018, 9, 17), 14],
// //                [Date.UTC(2018, 9, 18), 19],
// //                [Date.UTC(2018, 9, 19), 26],
// //                [Date.UTC(2018, 9, 20), 25],
// //                [Date.UTC(2018, 9, 21), 21],
// //                [Date.UTC(2018, 9, 22), 6],
// //                [Date.UTC(2018, 9, 23), 15],
// //                [Date.UTC(2018, 9, 24), 17],
// //                [Date.UTC(2018, 9, 25), 12],
// //                [Date.UTC(2018, 9, 26), 19],
// //                [Date.UTC(2018, 9, 27), 15],
// //                [Date.UTC(2018, 9, 28), 30],
// //                [Date.UTC(2018, 9, 29), 32],
// //                [Date.UTC(2018, 9, 30), 31],
// //                [Date.UTC(2018, 9, 31), 37]
// //            ]

// //        }]
// //     });
// // }
// // function LoadMacState() {
// //     $('#chart_macstate').highcharts({
// //         chart: {
// //             type: 'xrange'
// //         },
// //         title: {
// //             text: ''
// //         },
// //         xAxis: {
// //             gridLineWidth: 1,
// //             type: 'datetime',
// //             minRange: 2 * 3600000,//最小间隔
// //             tickInterval: 2 * 60 * 60 * 1000,
// //             dateTimeLabelFormats: {
// //                 hour: '%H:%M' //横坐标显示内容
// //             }
// //         },
// //         yAxis: {
// //             title: {
// //                 text: ''
// //             },
// //             categories: ['运行', '暂停', '维护', '故障'],
// //             reversed: true,

// //         },
// //         tooltip: {
// //             headerFormat: '<b>{series.name}</b><br>',
// //             pointFormat: '{point.x:%H:%M}~{point.x2:%H:%M}'
// //         },
// //         series: [{
// //             showInLegend: false,
// //             name: '运行',
// //             pointWidth: 20,
// //             data: [{
// //                 x: Date.UTC(2018, 11, 2, 8, 0),
// //                 x2: Date.UTC(2018, 11, 2, 8, 30),
// //                 y: 0
// //             }, {
// //                 x: Date.UTC(2018, 11, 2, 9, 0),
// //                 x2: Date.UTC(2018, 11, 2, 12, 0),
// //                 y: 0,
// //             }, {
// //                 x: Date.UTC(2018, 11, 2, 13, 0),
// //                 x2: Date.UTC(2018, 11, 2, 17, 0),
// //                 y: 0,
// //             }],
// //             dataLabels: {
// //                 useHTML:true,//执行html
// //                 formatter: function (
// //                 ) {
// //                     return this.point.name + this.point.url;//显示名称和图片
// //                 },
// //                 enabled: true
// //             }
// //         },
// //         {
// //             showInLegend: false,
// //             name: '暂停',
// //             pointWidth: 20,
// //             data: [{
// //                 x: Date.UTC(2018, 11, 2, 12, 0),
// //                 x2: Date.UTC(2018, 11, 2, 13, 0),
// //                 y: 1
// //             }],
// //             dataLabels: {
// //                 useHTML: true,//执行html
// //                 formatter: function (
// //                 ) {
// //                     return this.point.name + this.point.url;//显示名称和图片
// //                 },
// //                 enabled: true
// //             }
// //         },
// //         {
// //             showInLegend: false,
// //             name: '维护',
// //             pointWidth: 20,
// //             data: [{
// //                 x: Date.UTC(2018, 11, 2, 17, 0),
// //                 x2: Date.UTC(2018, 11, 2, 20, 0),
// //                 y: 2
// //             }],
// //             dataLabels: {
// //                 useHTML: true,//执行html
// //                 formatter: function (
// //                 ) {
// //                     return this.point.name + this.point.url;//显示名称和图片
// //                 },
// //                 enabled: true
// //             }
// //         },
// //         {
// //             showInLegend: false,
// //             name: '故障',
// //             pointWidth: 20,
// //             data: [{
// //                 x: Date.UTC(2018, 11, 2, 8, 30),
// //                 x2: Date.UTC(2018, 11, 2, 9, 0),
// //                 y: 3
// //             }],
// //             dataLabels: {
// //                 useHTML: true,//执行html
// //                 formatter: function (
// //                 ) {
// //                     return this.point.name + this.point.url;//显示名称和图片
// //                 },
// //                 enabled: true
// //             }
// //         },
// //         ]
// //     })
// // }

// // function LoadStep1() {
// //     if (typeof ($.fn.easyPieChart) === 'undefined') { return; }
// //     console.log('init_EasyPieChart213123');

// //     $('.chart').easyPieChart({
// //         easing: 'easeOutElastic',
// //         delay: 3000,
// //         barColor: '#26B99A',
// //         trackColor: '#808080',
// //         scaleColor: false,
// //         lineWidth: 20,
// //         trackWidth: 16,
// //         lineCap: 'butt',
// //         animate: 1,
// //         onStep: function (from, to, percent) {
// //             $(this.el).find('.percent').text(Math.round(percent));
// //         }
// //     });
// // var chart = window.chart = $('.chart').data('easyPieChart');
// // $('.js_update').on('click', function () {
// //     chart.update(Math.random() * 200 - 100);
// // });

// //hover and retain popover when on popover content
// // var originalLeave = $.fn.popover.Constructor.prototype.leave;
// // $.fn.popover.Constructor.prototype.leave = function (obj) {
// //     var self = obj instanceof this.constructor ?
// //         obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
// //     var container, timeout;

// //     originalLeave.call(this, obj);

// //     if (obj.currentTarget) {
// //         container = $(obj.currentTarget).siblings('.popover');
// //         timeout = self.timeout;
// //         container.one('mouseenter', function () {
// //             //We entered the actual popover – call off the dogs
// //             clearTimeout(timeout);
// //             //Let's monitor popover content instead
// //             container.one('mouseleave', function () {
// //                 $.fn.popover.Constructor.prototype.leave.call(self, self);
// //             });
// //         });
// //     }
// // };

// // $('body').popover({
// //     selector: '[data-popover]',
// //     trigger: 'click hover',
// //     delay: {
// //         show: 50,
// //         hide: 400
// //     }
// // });
// // }
// // function LoadStep2() {
// //     if (typeof ($.fn.easyPieChart) === 'undefined') { return; }
// //     console.log('init_EasyPieChart');

// //     $('.chart').easyPieChart({
// //         easing: 'easeOutElastic',
// //         delay: 3000,
// //         barColor: '#26B99A',
// //         trackColor: '#808080',
// //         scaleColor: false,
// //         lineWidth: 20,
// //         trackWidth: 16,
// //         lineCap: 'butt',
// //         onStep: function (from, to, percent) {
// //             $(this.el).find('.percent').text(Math.round(percent));
// //         }
// //     });
// //     var chart = window.chart = $('.chart').data('easyPieChart');
// //     $('.js_update').on('click', function () {
// //         chart.update(Math.random() * 200 - 100);
// //     });

// //     //hover and retain popover when on popover content
// //     var originalLeave = $.fn.popover.Constructor.prototype.leave;
// //     $.fn.popover.Constructor.prototype.leave = function (obj) {
// //         var self = obj instanceof this.constructor ?
// //             obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
// //         var container, timeout;

// //         originalLeave.call(this, obj);

// //         if (obj.currentTarget) {
// //             container = $(obj.currentTarget).siblings('.popover');
// //             timeout = self.timeout;
// //             container.one('mouseenter', function () {
// //                 //We entered the actual popover – call off the dogs
// //                 clearTimeout(timeout);
// //                 //Let's monitor popover content instead
// //                 container.one('mouseleave', function () {
// //                     $.fn.popover.Constructor.prototype.leave.call(self, self);
// //                 });
// //             });
// //         }
// //     };

// //     $('body').popover({
// //         selector: '[data-popover]',
// //         trigger: 'click hover',
// //         delay: {
// //             show: 50,
// //             hide: 400
// //         }
// //     });
// // }
// // function LoadStep3() {
// //     if (typeof ($.fn.easyPieChart) === 'undefined') { return; }
// //     console.log('init_EasyPieChart');

// //     $('.chart').easyPieChart({
// //         easing: 'easeOutElastic',
// //         delay: 3000,
// //         barColor: '#26B99A',
// //         trackColor: '#808080',
// //         scaleColor: false,
// //         lineWidth: 20,
// //         trackWidth: 16,
// //         lineCap: 'butt',
// //         onStep: function (from, to, percent) {
// //             $(this.el).find('.percent').text(Math.round(percent));
// //         }
// //     });
// //     var chart = window.chart = $('.chart').data('easyPieChart');
// //     $('.js_update').on('click', function () {
// //         chart.update(Math.random() * 200 - 100);
// //     });

// //     //hover and retain popover when on popover content
// //     var originalLeave = $.fn.popover.Constructor.prototype.leave;
// //     $.fn.popover.Constructor.prototype.leave = function (obj) {
// //         var self = obj instanceof this.constructor ?
// //             obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
// //         var container, timeout;

// //         originalLeave.call(this, obj);

// //         if (obj.currentTarget) {
// //             container = $(obj.currentTarget).siblings('.popover');
// //             timeout = self.timeout;
// //             container.one('mouseenter', function () {
// //                 //We entered the actual popover – call off the dogs
// //                 clearTimeout(timeout);
// //                 //Let's monitor popover content instead
// //                 container.one('mouseleave', function () {
// //                     $.fn.popover.Constructor.prototype.leave.call(self, self);
// //                 });
// //             });
// //         }
// //     };

// //     $('body').popover({
// //         selector: '[data-popover]',
// //         trigger: 'click hover',
// //         delay: {
// //             show: 50,
// //             hide: 400
// //         }
// //     });
// // }
// // function LoadStep4() {
// //     if (typeof ($.fn.easyPieChart) === 'undefined') { return; }
// //     console.log('init_EasyPieChart');

// //     $('.chart').easyPieChart({
// //         easing: 'easeOutElastic',
// //         delay: 3000,
// //         barColor: '#26B99A',
// //         trackColor: '#808080',
// //         scaleColor: false,
// //         lineWidth: 20,
// //         trackWidth: 16,
// //         lineCap: 'butt',
// //         onStep: function (from, to, percent) {
// //             $(this.el).find('.percent').text(Math.round(percent));
// //         }
// //     });
// //     var chart = window.chart = $('.chart').data('easyPieChart');
// //     $('.js_update').on('click', function () {
// //         chart.update(Math.random() * 200 - 100);
// //     });

// //     //hover and retain popover when on popover content
// //     var originalLeave = $.fn.popover.Constructor.prototype.leave;
// //     $.fn.popover.Constructor.prototype.leave = function (obj) {
// //         var self = obj instanceof this.constructor ?
// //             obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
// //         var container, timeout;

// //         originalLeave.call(this, obj);

// //         if (obj.currentTarget) {
// //             container = $(obj.currentTarget).siblings('.popover');
// //             timeout = self.timeout;
// //             container.one('mouseenter', function () {
// //                 //We entered the actual popover – call off the dogs
// //                 clearTimeout(timeout);
// //                 //Let's monitor popover content instead
// //                 container.one('mouseleave', function () {
// //                     $.fn.popover.Constructor.prototype.leave.call(self, self);
// //                 });
// //             });
// //         }
// //     };

// //     $('body').popover({
// //         selector: '[data-popover]',
// //         trigger: 'click hover',
// //         delay: {
// //             show: 50,
// //             hide: 400
// //         }
// //     });
// // }
// // function LoadStep5() {
// //     if (typeof ($.fn.easyPieChart) === 'undefined') { return; }
// //     console.log('init_EasyPieChart');

// //     $('.chart').easyPieChart({
// //         easing: 'easeOutElastic',
// //         delay: 3000,
// //         barColor: '#26B99A',
// //         trackColor: '#808080',
// //         scaleColor: false,
// //         lineWidth: 20,
// //         trackWidth: 16,
// //         lineCap: 'butt',
// //         onStep: function (from, to, percent) {
// //             $(this.el).find('.percent').text(Math.round(percent));
// //         }
// //     });
// //     var chart = window.chart = $('.chart').data('easyPieChart');
// //     $('.js_update').on('click', function () {
// //         chart.update(Math.random() * 200 - 100);
// //     });

// //     //hover and retain popover when on popover content
// //     var originalLeave = $.fn.popover.Constructor.prototype.leave;
// //     $.fn.popover.Constructor.prototype.leave = function (obj) {
// //         var self = obj instanceof this.constructor ?
// //             obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
// //         var container, timeout;

// //         originalLeave.call(this, obj);

// //         if (obj.currentTarget) {
// //             container = $(obj.currentTarget).siblings('.popover');
// //             timeout = self.timeout;
// //             container.one('mouseenter', function () {
// //                 //We entered the actual popover – call off the dogs
// //                 clearTimeout(timeout);
// //                 //Let's monitor popover content instead
// //                 container.one('mouseleave', function () {
// //                     $.fn.popover.Constructor.prototype.leave.call(self, self);
// //                 });
// //             });
// //         }
// //     };

// //     $('body').popover({
// //         selector: '[data-popover]',
// //         trigger: 'click hover',
// //         delay: {
// //             show: 50,
// //             hide: 400
// //         }
// //     });
// // }
// // function LoadStep6() {
// //     if (typeof ($.fn.easyPieChart) === 'undefined') { return; }
// //     console.log('init_EasyPieChart');

// //     $('.chart').easyPieChart({
// //         easing: 'easeOutElastic',
// //         delay: 3000,
// //         barColor: '#26B99A',
// //         trackColor: '#808080',
// //         scaleColor: false,
// //         lineWidth: 20,
// //         trackWidth: 16,
// //         lineCap: 'butt',
// //         onStep: function (from, to, percent) {
// //             $(this.el).find('.percent').text(Math.round(percent));
// //         }
// //     });
// //     var chart = window.chart = $('.chart').data('easyPieChart');
// //     $('.js_update').on('click', function () {
// //         chart.update(Math.random() * 200 - 100);
// //     });

// //     //hover and retain popover when on popover content
// //     var originalLeave = $.fn.popover.Constructor.prototype.leave;
// //     $.fn.popover.Constructor.prototype.leave = function (obj) {
// //         var self = obj instanceof this.constructor ?
// //             obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
// //         var container, timeout;

// //         originalLeave.call(this, obj);

// //         if (obj.currentTarget) {
// //             container = $(obj.currentTarget).siblings('.popover');
// //             timeout = self.timeout;
// //             container.one('mouseenter', function () {
// //                 //We entered the actual popover – call off the dogs
// //                 clearTimeout(timeout);
// //                 //Let's monitor popover content instead
// //                 container.one('mouseleave', function () {
// //                     $.fn.popover.Constructor.prototype.leave.call(self, self);
// //                 });
// //             });
// //         }
// //     };

// //     $('body').popover({
// //         selector: '[data-popover]',
// //         trigger: 'click hover',
// //         delay: {
// //             show: 50,
// //             hide: 400
// //         }
// //     });
// // }

// // var i=0;
// // function setStep1(){
// //     i=i+1;
// //     var chart = window.chart = $('#step1').data('easyPieChart');
// //     chart.update(i);
// // }

// $(function () {
//     // LoadMacState();
//     // LoadStep1();
//     // LoadStep2();
//     // LoadStep3();
//     // LoadStep4();
//     // LoadStep5();
//     // LoadStep6();
// });