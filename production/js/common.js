﻿var common = new Vue({
    data: {
        url: 'http://118.190.36.174:8080',
    },
    methods: {
        getStateName: function (stateCode) {
            switch (stateCode) {
                case 0:
                case 1:
                case 6:
                    return {
                        stateName: '停机中',
                            color: '#73879C',
                    };
                case 2:
                case 7:
                    return {
                        stateName: '运行中',
                            color: '#1ABB9C',
                    };
                case 3:
                case 4:
                case 5:
                    return {
                        stateName: '暂停中',
                            color: '#E6A23C',
                    };
                case 8:
                    return {
                        stateName: '故障中',
                            color: '#F56C6C',
                    };
            }
        },
    }
})