﻿const index = new Vue({
    el: '#app',
    data: {
        url: common.url,
        kanBan: null,
        jinBao: null,
    },
    created() {
        this.$nextTick(function () {
            this.getMachineRecord();
            this.getmachineAlarm();
        })
    },
    mounted() {},
    methods: {
        //看板数据获取
        getMachineRecord: function () {
            let _this = this;
            $.get(this.url + '/api/machineRecord/getMachineRecord', function (result) {
                for (var i = 0; i < result.Data.length; i++) {
                    result.Data[i].stateName = common.getStateName(result.Data[i].State).stateName;
                    result.Data[i].color = common.getStateName(result.Data[i].State).color;
                    result.Data[i].Percentage = result.Data[i].FinishedCount * 100 / (result.Data[i].FinishedCount + result.Data[i].SurplusCount);
                    if (!result.Data[i].SystemOpenedTime) {
                        result.Data[i].SystemOpenedTime = '0:00:00';
                    }
                }
                _this.kanBan = result.Data;
                _this.$nextTick(function () {
                    for (var i = 0; i < _this.kanBan.length; i++) {
                        _this.LoadStep(i, _this.kanBan[i].Percentage);
                    }
                })
            });
        },

        //警报数据获取
        getmachineAlarm: function () {
            let _this = this;
            $.get(this.url + "/api/machineAlarm/getmachineAlarm?Current=1&Row=6",
                function (result) {
                    _this.jinBao = result.Data;
                    console.log('警报数据', _this.jinBao);
                });
        },

        //easeOutElastic饼状图
        LoadStep: function (index, val) {
            if (typeof ($.fn.easyPieChart) === 'undefined') {
                return;
            }
            //此处保证加载custom慢导致没有图形
            $('#' + index).easyPieChart({
                easing: 'easeOutElastic',
                delay: 3000,
                barColor: '#26B99A',
                trackColor: '#808080',
                scaleColor: false,
                lineWidth: 20,
                trackWidth: 16,
                lineCap: 'butt',
                // animate: 1000,
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            var chart = window.chart = $('#' + index).data('easyPieChart');
            chart.update(Math.round(val));
        }
    }
})

$(document).ready(function () {
    $(".btn1").click(function () {
        $("p").slideToggle();
    });
});

setInterval(function () {
    index.getMachineRecord();
    index.getmachineAlarm();
}, 10000)